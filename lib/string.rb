# Public: Extended String Class to Replace vowel with given symbol.
#
# Examples
#
#   # puts "aeiouAEIOUABCDEF".replace_vowel('*')
#   # => "***********BCD*F"
#
# Print line with symbol in place of vowel

class String

  REGEX_VOWEL_PATTERN = /[aeiou]/i
  
  def replace_vowel(symbol = '*')
    gsub(REGEX_VOWEL_PATTERN, symbol)
  end

end

