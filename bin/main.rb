# Public: Replace vowel with * in a input text
#
# Examples
#
#   # ruby replace_vowel_with_asterisk.rb
#   # Enter a line: aeiouAEIOUABCDEF
#   # => "***********BCD*F"
#
# Print line with * in place of vowel

require_relative "../lib/string.rb"

print "Enter a line : "
puts gets.replace_vowel('*')

